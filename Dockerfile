FROM node:10.15.0-alpine
EXPOSE 8080 8081

WORKDIR /home/app

COPY package.json /home/app/
COPY package-lock.json /home/app/
COPY . /home/app

# RUN npm ci

RUN npm i -g nodemon

# Install app dependencies
RUN npm cache clear --force
ENV NPM_CONFIG_LOGLEVEL warn
RUN npm install --silent --progress=false
RUN npm install mongoose

ARG NODE_ENV
ARG DBURI


CMD ./scripts/start.sh